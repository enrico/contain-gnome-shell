
## Description

chrome-gnome-shell is a hard dependency of gnome-core, and it installs a
browser plugin that one may not want, and mandates its use by system-wide
chrome policies.

I consider having chrome-gnome-shell an unneeded increase of the attack
surface of my system, in exchange for the dubious privilege of being able to
download and execute, as my main user, random unreviewed code.

This package satifies the chrome-gnome-shell dependency, but installs nothing.

Note that after installing this package you need to purge chrome-gnome-shell
if it was previously installed, to have it remove its chromium policy files in
/etc/chromium


# Instructions

```
apt install equivs
equivs-build contain-gnome-shell
sudo dpkg -i contain-gnome-shell_1.0_all.deb
sudo dpkg --purge chrome-gnome-shell
```
